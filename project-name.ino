const int redPin = 11;
const int greenPin = 9;
const int bluePin = 10;
void setup()
{
  Serial.begin(9600);
  
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}

void loop()
{
  if (Serial.available() > 0) {
    String input = Serial.readStringUntil('\n');
    input.trim();
    
    if(input == "r") {
      digitalWrite(redPin, 1);
      digitalWrite(greenPin, 0);
    digitalWrite(bluePin, 0);
    }
    else if(input == "g") {
      digitalWrite(redPin, 0);
      digitalWrite(greenPin, 1);
    digitalWrite(bluePin, 0);
    }
    else if (input == "b") {
      digitalWrite(redPin, 0);
      digitalWrite(greenPin, 0);
    digitalWrite(bluePin, 1);
    }
    else if(input == "on") {
      digitalWrite(redPin, 1);
      digitalWrite(greenPin, 1);
    digitalWrite(bluePin, 1);
    }
    else if(input == "off") {
      digitalWrite(redPin, 0);
      digitalWrite(greenPin, 0);
    digitalWrite(bluePin, 0);
    }
  }
}
